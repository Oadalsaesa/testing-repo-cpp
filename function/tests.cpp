#include <gtest/gtest.h>
#include <functional>
#include <numeric>
#include <vector>

#include "function.hpp"

struct Helper {
  int method_1(int z) { return x + z; }
  int method_2(int z) { return y + z; }

  int x = 3;
  int y = 30;
};

int sum(int x, int y) {
  return x + y;
}

int multiply(int x, int y) {
  return x * y;
}

template <typename R, typename T>
R template_test_function(T t) {
  return static_cast<R>(t);
}

using namespace std::placeholders;

TEST(basic_usage, constructors) {
  {
    Function<void()> func;
    ASSERT_THROW(func(), BadFunctionCall);
  }

  {
    Function<int(int, int)> func = sum;
    ASSERT_EQ(func(5, 10), 15);
  }

  {
    Function<int(double)> func = template_test_function<double, int>;
    ASSERT_EQ(func(123.456), 123);
  }

  auto lambda = [](int a, int b, int c, int d){
    return a * b + c * d;
  };

  {
    Function<int(int, int, int, int)> func = lambda;
    ASSERT_EQ(func(2, 3, 20, 30), 606);
  }

  auto func_ptr = +[]() {
    return 10;
  };

  {
    Function<int()> func = func_ptr;
    ASSERT_EQ(func(), 10);
  }

  {
    Function<int(int, int)> func = std::bind(lambda, 2, _2, _1, 30);
    ASSERT_EQ(func(20, 3), 606);
  }

  {
    Helper helper;

    Function<int(Helper&, int)> func = &Helper::method_1;
    ASSERT_EQ(func(helper, 10), 13);

    Function<int&(Helper&)> attr = &Helper::x;
    ASSERT_EQ(attr(helper), 3);

    attr(helper) = 10;
    ASSERT_EQ(func(helper, 10), 20);
  }
}

TEST(basic_usage, assignment) {
  {
    Function<int(int, int)> func = sum;
    ASSERT_EQ(func(5, 10), 15);
    func = multiply;
    ASSERT_EQ(func(5, 10), 50);
  }

  {
    Function<int(int, int)> func_1 = sum;
    Function<int(int, int)> func_2 = multiply;
    ASSERT_EQ(func_1(5, 10), 15);

    func_1 = func_2;
    ASSERT_EQ(func_1(5, 10), 50);
    ASSERT_EQ(func_2(5, 10), 50);

    func_1 = std::move(func_2);
    ASSERT_EQ(func_1(5, 10), 50);
    ASSERT_THROW(func_2(5, 10), BadFunctionCall);

    ASSERT_EQ(std::move(func_1)(5, 10), 50);
    ASSERT_EQ(func_1(5, 10), 50);
  }

  {
    Helper helper;
    Function<int(Helper&, int)> func = &Helper::method_1;
    Function<int&(Helper&)> attr = &Helper::x;

    ASSERT_EQ(func(helper, 10), 13);
    attr(helper) = 10;
    ASSERT_EQ(func(helper, 10), 20);

    func = &Helper::method_2;
    ASSERT_EQ(func(helper, 10), 40);

    attr = &Helper::y;
    ASSERT_EQ(attr(helper), 30);

    attr(helper) = 100;
    ASSERT_EQ(func(helper, 10), 110);
  }
}

TEST(basic_usage, comparation) {
  Function<int(int, int)> func;
  ASSERT_EQ(func, nullptr);
  ASSERT_FALSE(func);

  func = multiply;
  ASSERT_NE(func, nullptr);
  ASSERT_TRUE(func);

  auto f = std::move(func);
  ASSERT_EQ(func, nullptr);
  ASSERT_FALSE(func);
}

TEST(basic_usage, invokable) {
  Function<int(int, int)> func = sum;
  ASSERT_EQ(std::invoke(func, 5, 10), 15);
}

TEST(lambdas, captures) {
  {
    std::vector<int> vec = {1, 2, 3};
    Function<int()> func = [&]() {
      return std::accumulate(vec.begin(), vec.end(), 0);
    };

    ASSERT_EQ(func(), 6);
    vec.push_back(4);
    ASSERT_EQ(func(), 10);

    Function<int()> func2 = std::move(func);
    ASSERT_EQ(func2(), 10);
  }
}

TEST(lambdas, CTAD) {
  {
    Function func = sum;
    ASSERT_EQ(func(5, 10), 15);
  }
  {
    Function func = [](int x) { return x * x * x; };
    ASSERT_EQ(func(10), 1000);
  }
}

TEST(lambdas, recursive) {
  Function<int(int)> func = [&](int x) {
    return x == 0 ? 1 : x * func(x - 1);
  };
  ASSERT_EQ(func(5), 120);
}

TEST(swap, swap) {
  Function<int(int, int)> func1 = sum;
  Function<int(int, int)> func2 = multiply;
  ASSERT_EQ(func1(5, 10), 15);
  ASSERT_EQ(func2(5, 10), 50);

  func1.swap(func2);
  ASSERT_EQ(func1(5, 10), 50);
  ASSERT_EQ(func2(5, 10), 15);

  std::swap(func1, func2);
  ASSERT_EQ(func1(5, 10), 15);
  ASSERT_EQ(func2(5, 10), 50);
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
